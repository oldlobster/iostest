//
//  ViewController.swift
//  Autolayout
//
//  Created by david lobo on 30/01/2016.
//  Copyright © 2016 David Lobo. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate  {

    var formCenterOriginal: CGFloat = 0;
    
    @IBOutlet weak var centerConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.formCenterOriginal = self.centerConstraint.constant;
        
        // register for the keyboard event notifications
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func keyboardWillShow(notification: NSNotification) {
        var info = notification.userInfo!
        
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue();
        //print("keyboardWillShow: \(self.formCenterOriginal) | \(keyboardFrame.size.height)")
        
        UIView.animateWithDuration(0.1, animations: { () -> Void in
            
            // move the center constraint based on the keyboard height
            self.centerConstraint.constant = self.formCenterOriginal - keyboardFrame.size.height / 2
        });
    }
    
    func keyboardWillHide(notification: NSNotification) {
        // reset the center constraint
        self.centerConstraint.constant = self.formCenterOriginal;
    }

    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true);
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}

