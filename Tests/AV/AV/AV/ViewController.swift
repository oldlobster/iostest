//
//  ViewController.swift
//  AV
//
//  Created by david lobo on 31/01/2016.
//  Copyright © 2016 David Lobo. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

    @IBOutlet weak var takePhotoButton: UIButton!
    @IBOutlet weak var previewView: UIView!
    
    var captureSession: AVCaptureSession?
    var stillImageOutput: AVCaptureStillImageOutput?
    var previewLayer: AVCaptureVideoPreviewLayer?
    
    var captureOrientation: AVCaptureVideoOrientation?
    var imageOrientation: UIImageOrientation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCaptureSession()
    }
    
    /**
     * Setup a capture session using the front camera
     *
     */
    func setupCaptureSession() {
        captureSession = AVCaptureSession()
        captureSession!.sessionPreset = AVCaptureSessionPresetPhoto
        
        var frontCamera: AVCaptureDevice? = nil
        let videoDevices = AVCaptureDevice.devicesWithMediaType(AVMediaTypeVideo)
        
        // find the front camera device
        for device in videoDevices{
            let device = device as! AVCaptureDevice
            if device.position == AVCaptureDevicePosition.Front {
                frontCamera = device
                break
            }
        }
        
        
        if let frontCamera = frontCamera {
            var error: NSError?
            var input: AVCaptureDeviceInput!
            do {
                input = try AVCaptureDeviceInput(device: frontCamera)
            } catch let error1 as NSError {
                error = error1
                input = nil
            }
            
            if error == nil && captureSession!.canAddInput(input) {
                captureSession!.addInput(input)
                
                stillImageOutput = AVCaptureStillImageOutput()
                stillImageOutput!.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
                if captureSession!.canAddOutput(stillImageOutput) {
                    captureSession!.addOutput(stillImageOutput)
                    
                    previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
                    previewLayer!.videoGravity = AVLayerVideoGravityResizeAspect
                    previewLayer!.connection?.videoOrientation = AVCaptureVideoOrientation.Portrait
                    previewView.layer.addSublayer(previewLayer!)
                    
                    captureSession!.startRunning()
                }
            }
        } else {
            // print("No front camera is available")
            
            // Disable the button as camera is unavailable
            self.takePhotoButton.enabled = false
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if let previewLayer = previewLayer {
            previewLayer.frame = previewView.bounds
        } else {
            // If theres no camera - display an error alert
            let alertController = UIAlertController(title: "No front camera", message: "Please ensure you run on a device with front camera", preferredStyle: .Alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
                // Dismiss the alert
            }
            alertController.addAction(OKAction)
            
            presentViewController(alertController, animated: true, completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        let orientation: UIDeviceOrientation = UIDevice.currentDevice().orientation

        // Resizing the preview layer when the device orientation changes
        if let previewLayer = previewLayer {
            self.previewLayer!.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - (self.takePhotoButton.bounds.height + 7));
        
            switch (orientation) {
            case .Portrait:
                previewLayer.connection.videoOrientation = AVCaptureVideoOrientation.Portrait
                captureOrientation = AVCaptureVideoOrientation.Portrait
                imageOrientation = UIImageOrientation.Right
                print("Portrait")
                break
            case .LandscapeRight:
                previewLayer.connection.videoOrientation = AVCaptureVideoOrientation.LandscapeLeft
                captureOrientation = AVCaptureVideoOrientation.LandscapeRight
                imageOrientation = UIImageOrientation.Up
                print("LandscapeRight")
                break
            case .LandscapeLeft:
                previewLayer.connection.videoOrientation = AVCaptureVideoOrientation.LandscapeRight
                captureOrientation = AVCaptureVideoOrientation.LandscapeLeft
                imageOrientation = UIImageOrientation.Down
                
                print("LandscapeLeft")
                break
            default:
                previewLayer.connection.videoOrientation = AVCaptureVideoOrientation.Portrait
                captureOrientation = AVCaptureVideoOrientation.Portrait
                imageOrientation = UIImageOrientation.Right
                print("default")
                break
            }
        }
    }
    
    func image(image: UIImage, didFinishSavingWithError error: NSErrorPointer, contextInfo: UnsafePointer<()>) {
        dispatch_async(dispatch_get_main_queue(), {
            
            let alertController = UIAlertController(title: "Success", message: "The selfie has been saved to your camera Roll", preferredStyle: .Alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
                // Dismiss the alert
            }
            alertController.addAction(OKAction)
            
            self.presentViewController(alertController, animated: true, completion: nil)
        })
    }
    
    @IBAction func didPressTakePhoto(sender: UIButton) {
        if let videoConnection = stillImageOutput!.connectionWithMediaType(AVMediaTypeVideo) {

            videoConnection.videoOrientation = self.captureOrientation!
            stillImageOutput?.captureStillImageAsynchronouslyFromConnection(videoConnection, completionHandler: {(sampleBuffer, error) in
                if (sampleBuffer != nil) {
                    let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(sampleBuffer)
                    let dataProvider = CGDataProviderCreateWithCFData(imageData)
                    let cgImageRef = CGImageCreateWithJPEGDataProvider(dataProvider, nil, true, CGColorRenderingIntent.RenderingIntentDefault)
                    
                    let image = UIImage(CGImage: cgImageRef!, scale: 1.0, orientation: self.imageOrientation!)
                    
                    UIImageWriteToSavedPhotosAlbum(image, self, Selector("image:didFinishSavingWithError:contextInfo:"), nil)
                }
            })
        }
    }
}