//
//  Task+CoreDataProperties.swift
//  Networking
//
//  Created by david lobo on 29/01/2016.
//  Copyright © 2016 David Lobo. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Task {

    @NSManaged var complete: NSNumber?
    @NSManaged var image: String?
    @NSManaged var title: String?
    @NSManaged var imageId: NSNumber?

}
