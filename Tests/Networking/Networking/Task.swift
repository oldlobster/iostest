//
//  Task.swift
//  Networking
//
//  Created by david lobo on 28/01/2016.
//  Copyright © 2016 David Lobo. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class Task: NSManagedObject {
    
    var hasImage: Bool {
        return imageId != nil
    }
    
    var thumbnailImage: UIImage {
        if hasImage {
            return UIImage(contentsOfFile: imagePath)!
        }
        
        return UIImage(named: "unknown_30")!
    }
    
    class func nextImageId() -> Int {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        let currentId = userDefaults.integerForKey("ImageId")
        
        userDefaults.setInteger(currentId + 1, forKey: "ImageId")
        userDefaults.synchronize()
        return currentId
    }
    
    var imagePath: String {
        assert(imageId != nil, "No image Id set")
        let filename = "Image-\(imageId!.integerValue).png"
        return (applicationDocumentsDirectoryPath as NSString).stringByAppendingPathComponent(filename)
        
    }
    
    func removeImageFile() {
        if hasImage {
            let path = imagePath
            let fileManager = NSFileManager.defaultManager()
            if fileManager.fileExistsAtPath(path) {
                print("file exists")
                do {
                    try fileManager.removeItemAtPath(path)
                    
                } catch {
                    print("Error removing file: \(error)")
                }
            }
        }
    }
    
    override func prepareForDeletion() {
        super.prepareForDeletion()
        print("deleting: \(imagePath)")
        removeImageFile()
    }
}
