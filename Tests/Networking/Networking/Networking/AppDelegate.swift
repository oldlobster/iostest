//
//  AppDelegate.swift
//  Networking
//
//  Created by david lobo on 28/01/2016.
//  Copyright © 2016 David Lobo. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        let navController = self.window!.rootViewController as! UINavigationController
        let rootViewController = navController.viewControllers[0] as! TaskViewController
        
        rootViewController.context = self.managedObjectContext;
        
        // simple flag that ensures download is only completed once
        // in future this would be extended to update when needed
        if isUpdateRequired() {
            
            // use a background thread to stop UI blocking
            let privateContext = NSManagedObjectContext(concurrencyType: .PrivateQueueConcurrencyType)
            privateContext.persistentStoreCoordinator = managedObjectContext.persistentStoreCoordinator
            privateContext.performBlock({ Void in
                
                // update the data model and images
                self.deleteTasks(privateContext)
                self.fetchTasks(privateContext)
                
                dispatch_async(dispatch_get_main_queue()) {
                    // update the UI on main thread
                    rootViewController.updateTable()
                }
            })
        } else {
            rootViewController.updateTable()
        }
    
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.davidlobo.Networking" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1]
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("Networking", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
            
        print(url)
        do {
            try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason

            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()

    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
}

extension AppDelegate {
    
    /**
     * Fetch the tasks from the server and download/resize the images
     *
     * - Parameter managedObjectContext: the NSManagedObjectContext to use
     *
     * - Returns: nil
     */
    func fetchTasks(context: NSManagedObjectContext) {
        let endpoint = NSURL(string: "https://bitbucket.org/fatunicorn/iostest/raw/78f902ddb31ac96bb23b901054442dee52664271/data/tasklist.json")
        let data = NSData(contentsOfURL: endpoint!)
        
        var json: [AnyObject]
        do {
            json = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments) as! [AnyObject]
            //print(json)
            let tasks = json
            
            for taskData in tasks {
                
                var complete = false
                var image: String? = nil
                var title: String? = nil
                
                if let _ = taskData["task"] {
                    
                    title = taskData["task"] as? String
                    
                    if let _ = taskData["done"] {
                        complete = taskData["done"] as! Bool
                    }
                    
                    if let _ = taskData["image"] {
                        image = taskData["image"] as? String
                    }
                    
                    // create a new task and assign the properties
                    let newTask = NSEntityDescription.insertNewObjectForEntityForName("Task", inManagedObjectContext: context) as! Task
                    
                    newTask.title = title
                    newTask.complete = complete
                    newTask.image = image
                    
                    // download and resize the image
                    var newImage: UIImage? = nil
                    if let url = NSURL(string: image!) {
                        if let data = NSData(contentsOfURL: url) {
                            newImage = UIImage(data: data)
                            
                            newImage = newImage!.resizeImageToWidth(50)
                        }
                    }
                    
                    // save the image and assign an id
                    if let newImage = newImage {
                        if let data = UIImagePNGRepresentation(newImage) {
                            
                            let imageId = Task.nextImageId()
                            newTask.imageId = imageId
                            //print("nextPhotoID: \(imageId)")
                            
                            data.writeToFile(newTask.imagePath, atomically: true)
                        }
                    }
                }
            }
            
            // save context at the end of processing
            do {
                try context.save()
            } catch let error as NSError  {
                print("Could not save \(error), \(error.userInfo)")
            }
        } catch {
            print("Error fetchTasks(): \(error)")
        }
        
        // set flag to so that next time data is not loaded from remote
        setUpdateRequired(false)
    }
    
    /**
     * Delete all the Task objects
     *
     * - Parameter managedObjectContext: the NSManagedObjectContext to use
     *
     * - Returns: nil
     */
    func deleteTasks(context: NSManagedObjectContext) {
        
        let fetchRequest = NSFetchRequest(entityName: "Task")
        
        do {
            let fetchedEntities = try context.executeFetchRequest(fetchRequest) as! [Task]
            
            for entity in fetchedEntities {
                context.deleteObject(entity)
            }
        } catch {
            print("Error deleting: \(error)")
        }
        
        do {
            try context.save()
        } catch {
            print("Error saving: \(error)")
        }
    }
}