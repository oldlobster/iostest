//
//  Functions.swift
//  Networking
//
//  Created by david lobo on 29/01/2016.
//  Copyright © 2016 David Lobo. All rights reserved.
//

import Foundation

let updateRequiredKey = "updateRequired"

let applicationDocumentsDirectoryPath: String = {
    
    let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
    
    return paths[0]
}()

// is remote update needed
func isUpdateRequired() -> Bool {
    let defaults = NSUserDefaults.standardUserDefaults()
    if let _ = defaults.objectForKey(updateRequiredKey) {
        // this this means key is set
        let updateRequired = defaults.boolForKey(updateRequiredKey)
        
        //print("UpdateRequired set: \(updateRequired)")
        return updateRequired
        
    } else {
        // this means key isn't set yet
        //print("UpdateRequired not set")
        return true
    }
}

// set flag for remote update required
func setUpdateRequired(updateRequired: Bool) {
    let defaults = NSUserDefaults.standardUserDefaults()
    defaults.setObject(updateRequired, forKey: updateRequiredKey)
}