//
//  TaskViewController.swift
//  Networking
//
//  Created by david lobo on 29/01/2016.
//  Copyright © 2016 David Lobo. All rights reserved.
//

import UIKit
import CoreData

class TaskViewController: UITableViewController, NSFetchedResultsControllerDelegate {
    var context: NSManagedObjectContext!
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    var predicateToUse: NSPredicate?
    let reuseIdentifier = "TaskCell"
    
    // used to filter the table
    let predicates = [
        "complete": NSPredicate(format: "complete == 1"),
        "todo": NSPredicate(format: "complete == 0"),
        "all": NSPredicate(format: "complete == 0 || complete == 1")
    ]
    
    lazy var fetchedResultsController: NSFetchedResultsController = {
        print("setting fetchResultsController")
        let tasksFetchRequest = NSFetchRequest(entityName: "Task")
        let primarySortDescriptor = NSSortDescriptor(key: "title", ascending: true)
        
        tasksFetchRequest.sortDescriptors = [primarySortDescriptor]
        
        let frc = NSFetchedResultsController(
            fetchRequest: tasksFetchRequest,
            managedObjectContext: self.context,
            sectionNameKeyPath: nil,
            cacheName: nil)
        
        frc.delegate = self
        
        return frc
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
     func updateTable() {
        do {
            try fetchedResultsController.performFetch()
        } catch {
            print("An error occurred")
        }
        
        tableView.reloadData()
    }
}

extension TaskViewController {
    
    @IBAction func segmentedControlChanged(sender: AnyObject) {
        if(segmentedControl.selectedSegmentIndex == 0)
        {
            print( "First Segment Selected")
            
            self.fetchedResultsController.fetchRequest.predicate = self.predicates["todo"]
            
            do {
                try fetchedResultsController.performFetch()
            } catch {
                print("Error fetching \(error)")
            }
            
            tableView.reloadData()
        } else if(segmentedControl.selectedSegmentIndex == 1) {
            print("Second Segment Selected")
            
            self.fetchedResultsController.fetchRequest.predicate = self.predicates["complete"]
            
            do {
                try fetchedResultsController.performFetch()
            } catch {
                print("Error fetching \(error)")
            }
            
            tableView.reloadData()
        } else if(segmentedControl.selectedSegmentIndex == 2) {
            print("Third Segment Selected")
            
            self.fetchedResultsController.fetchRequest.predicate = self.predicates["all"]
            
            do {
                try fetchedResultsController.performFetch()
            } catch {
                print("Error fetching \(error)")
            }
            
            tableView.reloadData()
        }
    }
}

extension TaskViewController {
    // MARK: TableView Data Source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {

        if let sections = fetchedResultsController.sections {
            return sections.count
        }
        
        return 0
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let sections = fetchedResultsController.sections {
            let currentSection = sections[section]
            return currentSection.numberOfObjects
        }
        
        return 0
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:UITableViewCell? = self.tableView?.dequeueReusableCellWithIdentifier(reuseIdentifier)
        
        if cell == nil {
            //cell = TaskCell(style: .Value1, reuseIdentifier: reuseIdentifier)
            cell = UITableViewCell(style: .Value1, reuseIdentifier: reuseIdentifier)
        }
        return cell!
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
            let task = fetchedResultsController.objectAtIndexPath(indexPath) as! Task
            configureCellForTask(cell, task: task)
    }
    
    func configureCellForTask(cell: UITableViewCell, task: Task) -> UITableViewCell {
        
        cell.textLabel?.text = task.title
        cell.imageView?.image = task.thumbnailImage
 
        var frame = cell.imageView!.frame
        let imageSize = 30 as CGFloat
        frame.size.height = imageSize
        frame.size.width  = imageSize
        cell.imageView!.frame = frame
        cell.imageView!.layer.cornerRadius = imageSize / 2.0
        cell.imageView!.clipsToBounds = true
        
        
        if task.complete == true {
            cell.accessoryType = .Checkmark
        } else {
            cell.accessoryType = .None
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        

        if let sections = fetchedResultsController.sections {
            let currentSection = sections[section]
            return currentSection.name
        }
        
        return nil
    }
}
